# Word Cloud Panel [Moved to Github]

This is no longer mantained since I've upgraded everything to Grafana 7 and moved the code to github.

## Yo can find the new version here: https://github.com/raulsperoni/magnesium-wordcloud-panel

Basado en https://react-wordcloud.netlify.com/

First, install dependencies:

```BASH
yarn install
```

To work with this plugin run:

```BASH
yarn dev
```

or

```BASH
yarn watch
```

This will run linting tools and apply prettier fix.

To build the plugin run:

```BASH
yarn build
```
